describe('Home page', function(){
  it('title should contain AdminPanel', function(){
    browser.get('/');
    expect(browser.getTitle()).toMatch(/.*AdminPanel.*/);
  });

  it('should have no access to categories for unauthorized user', function(){
    browser.get('/categories');
    var url = browser.getCurrentUrl();
    expect(url).toMatch(/.*auth\/login.*/);
  });

  it('should have an opportunity to log in successfully', function(){
    browser.get('/auth/login');
    element(by.id('email_block')).sendKeys('admin@admin.ua');
    element(by.id('password_block')).sendKeys('123456789');
    element(by.css('.submit_button')).click();

    browser.get('/categories');
    var url = browser.getCurrentUrl();
    expect(url).toMatch(/.*auth\/login.*/);
  });
  
});