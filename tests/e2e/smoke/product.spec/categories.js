describe('Categories', function() {
    it('should add a category successfully', function () {
        /*
         1. Precondition: admin should be logged in
         2. Steps to be executed:
         1) enter a name of the category
         2) click on the "Add" button
         3. Expected result: one more category is
         added to the categories list
         */
    });

    it('should fail to add category without entering its name', function () {
        /*
         1. Precondition: admin should be logged in
         2. Steps to be executed:
         1) click on the "Add" button
         3. Expected result: proper error must be
         displayed and prompt to enter a name
         */
    });

    it('should remove a category successfully', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         2. Steps to be executed:
         1) click on the "Remove" button of the respective category
         2) click on the "Ok" button on the popup window
         3. Expected result: a respective category is removed
         */
    });

    it('should cancel removing a category', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         2. Steps to be executed:
         1) click on the "Remove" button of the respective category
         2) click on the "Cancel" button on the popup window
         3. Expected result: a respective category isn't removed
         */
    });

    it('should edit a category successfully without changes', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         2. Steps to be executed:
         1) click on the "Edit" button of the respective category
         2) click on the "Save" button
         3. Expected result: a respective category is saved without changes
         and admin is navigated back to the categories list page
         */
    });

    it('should edit a category successfully, changing the category-s name', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         2. Steps to be executed:
         1) click on the "Edit" button of the respective category
         2) edit the name
         2) click on the "Save" button
         3. Expected result: a respective category's name is edited
         */
    });

    it('should edit a category successfully, changing the category-s name', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         2. Steps to be executed:
         1) click on the "Edit" button of the respective category
         2) click on the "Choose Files" button
         3) choose the appropriate folder
         4) choose the appropriate png picture
         5) click on the "Open" button
         6) click on the "Save" button
         3. Expected result: a respective category's picture is edited
         */
    });

    it('should return to the categories list after saving a category-s edition', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         3) "Save" button should be clicked on on the edition page
         2. Steps to be executed:
         1) Click the "Ok" button on the popup message
         3. Expected result: admin is navigated back
         to the categories list page
         */
    });

    it('should make the category unpublished', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         3) category should be published
         2. Steps to be executed:
         1) click on the "Status" checkmark of the respective category's
         3. Expected result: the category gets unpublished status
         */
    });

    it('should make the category published', function () {
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         3) category should be unpublished
         2. Steps to be executed:
         1) click on the "Status" checkmark of the respective category's
         3. Expected result: the category gets published status
         */
    });

});

  