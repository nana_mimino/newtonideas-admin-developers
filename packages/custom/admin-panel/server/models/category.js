/**
 * Created by Tania on 21/10/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
    _   = require('lodash');

/**
 * Getter
 */
var escapeProperty = function(value) {
    return _.escape(value);
};

/**
 * Product Schema
 */

var Product = new Schema({
    category_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true,
        get: escapeProperty
    },
    icon: {
        type: String
    },
    status: {
        type: String,
        enum: ['published', 'unpublished'],
        required: true,
        get: escapeProperty
    }
});

/**
 * Category Schema
 */

var CategorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        get: escapeProperty
    },
    icon: {
        type: String
    },
    status: {
        type: String,
        enum: ['published', 'unpublished'],
        required: true,
        get: escapeProperty
    }
});

mongoose.model('Product', Product);
mongoose.model('Category', CategorySchema);