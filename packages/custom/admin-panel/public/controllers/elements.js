/**
 * Created by Tania on 4/12/2015.
 */

'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel')
    .controller('EditElementController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.global = Global;
            $scope.url = window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts =$scope.url.split('/');
            $scope.category_id = urlParts[4];
            $scope.element_id = urlParts[urlParts.length - 3];

            var refresh = function() {
                $http.get('/api/element-edit/' + $scope.element_id).success(function (response) {
                    $scope.element = response;
                    $scope.errorMsg = '';
                    $scope.name = response.name;
                });
            };
            refresh();

            $scope.saveElement = function(){
                if (!$scope.element.name || !$scope.element.z_index) {
                    $scope.errorMsg = "Please, enter name and z-index of this element";
                    return;
                }
                if ($scope.element.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.element.name = '';
                    return;
                }
                if (isNaN($scope.element.z_index)) {
                    $scope.errorMsg = "Please, enter z_index in numerals";
                    $scope.element.z_index = '';
                    return;
                }
                $http.post('api/element-edit/' + $scope.element_id, $scope.element).success(function(response){
                    refresh();
                    $scope.saved = response;
                });
            };
        }
    ])
    .controller('ElementsController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.global = Global;
            $scope.url = window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts = $scope.url.split('/');
            $scope.product_id = urlParts[urlParts.length - 2];
            var publishableElements = function() {
                $http.get('/api/element-publishable/').success(function (response) {
                    $scope.publishable = response;
                });
            };

            var refresh = function() {
                $http.get('/api/product/' + $scope.product_id).success(function (response) {
                    $scope.elements = response;
                    $scope.element = '';
                    $scope.errorMsg = '';
                });
                $http.get('/api/product-name/' + $scope.product_id).success(function (response) {
                    $scope.product_name = response;
                });
                publishableElements();
            };
            refresh();

            $scope.addElement = function(){
                if ($scope.element.name === undefined) {
                    $scope.errorMsg = "Please, enter name of new element";
                    return;
                }
                if ($scope.element.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.element.name = '';
                    return;
                }
                if (isNaN($scope.element.z_index)) {
                    $scope.errorMsg = "Please, enter z_index in numerals";
                    $scope.element.z_index = '';
                    return;
                }
                $http.post('api/product/' + $scope.product_id, $scope.element).success(function(response){
                    refresh();
                    $scope.added = response;
                    $scope.removed = false;
                });
            };

            $scope.removeElement = function(id, name){
                if (confirm('Delete element \''+name+'\'?') === true) {
                    $http.delete('api/product/' + id).success(function(response){
                        refresh();
                        $scope.added = false;
                        $scope.removed = name;
                    });
                }
            };

            $scope.unpublish = function(element){
                $http.post('/api/element-unpublish', element).success(function(response){
                    refresh();
                });
            };
            $scope.publish = function(element){
                $http.post('/api/element-publish', element).success(function(response){
                    refresh();
                });
            };

            $scope.hasPublishedElementVariations = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            }

            $scope.movable = function(element){
                $http.post('/api/element-movable', element).success(function(response){
                    refresh();
                });
            };
            $scope.immovable = function(element){
                $http.post('/api/element-immovable', element).success(function(response){
                    refresh();
                });
            };

        }
    ]);

