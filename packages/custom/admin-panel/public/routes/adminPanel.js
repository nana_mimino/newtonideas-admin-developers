'use strict';

angular.module('mean.admin-panel').config(['$stateProvider' , '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('adminPanel example page', {
                url: '/adminPanel',
                templateUrl: 'admin-panel/views/index.html'
            })
            .state('adminPanel Categories', {
                url: '/categories',
                templateUrl: 'admin-panel/views/categories.html',
                ncyBreadcrumb: {
                    label: 'Categories'
                }
            })
            .state('adminPanel Category edit', {
                url: '/categories/:category_id/edit',
                templateUrl: 'admin-panel/views/editCategory.html',
                ncyBreadcrumb: {
                    label: 'Edit',
                    parent: 'adminPanel Categories'
                }
            })
            .state('adminPanel Product edit', {
                url: '/categories/:category_id/:product_id/edit',
                templateUrl: 'admin-panel/views/editProduct.html',
                ncyBreadcrumb: {
                    label: 'Edit',
                    parent: 'adminPanel Products'
                }
            })
            .state('adminPanel Categories publish', {
                url: '/categories-publish',
                templateUrl: 'admin-panel/views/categories.html'
            })
            .state('adminPanel Categories unpublish', {
                url: '/categories-unpublish',
                templateUrl: 'admin-panel/views/categories.html'
            })
            .state('adminPanel Category can be published', {
                url: '/category-publishable',
                templateUrl: 'admin-panel/views/products.html'
            })

            .state('adminPanel Products', {
                url: '/categories/:category_id',
                templateUrl: 'admin-panel/views/products.html',
                ncyBreadcrumb: {
                    label: 'Products',
                    parent: 'adminPanel Categories'
                }
            })
            .state('adminPanel Products publish', {
                url: '/product-publish',
                templateUrl: 'admin-panel/views/products.html'
            })
            .state('adminPanel Products unpublish', {
                url: '/product-unpublish',
                templateUrl: 'admin-panel/views/products.html'
            })
            .state('adminPanel Product can be published', {
                url: '/product-publishable',
                templateUrl: 'admin-panel/views/elements.html'
            })
            .state('adminPanel Category name', {
                url: '/category-name',
                templateUrl: 'admin-panel/views/products.html'
            })
            .state('adminPanel add default admin', {
                url: '/adminPanel/initialization',
                templateUrl: 'admin-panel/views/defaultAdmin.html'
            })
            .state('adminPanel Elements', {
                url: '/categories/:category_id/:product_id',
                templateUrl: 'admin-panel/views/elements.html',
                ncyBreadcrumb: {
                    label: 'Elements',
                    parent: 'adminPanel Products'
                }
            })
            .state('adminPanel Element edit', {
                url: '/categories/:category_id/:product_id/:element_id/edit',
                templateUrl: 'admin-panel/views/editElement.html',
                ncyBreadcrumb: {
                    label: 'Edit',
                    parent: 'adminPanel Elements'
                }
            })
            .state('adminPanel Product name', {
                url: '/product-name',
                templateUrl: 'admin-panel/views/elements.html'
            })

            .state('adminPanel ElementVariations', {
                url: '/categories/:category_id/:product_id/:element_id',
                templateUrl: 'admin-panel/views/elementVariations.html',
                ncyBreadcrumb: {
                    label: 'Variations',
                    parent: 'adminPanel Elements'
                }
            })
            .state('adminPanel Elements publish', {
                url: '/element-publish',
                templateUrl: 'admin-panel/views/elements.html'
            })
            .state('adminPanel Elements unpublish', {
                url: '/element-unpublish',
                templateUrl: 'admin-panel/views/elements.html'
            })
            .state('adminPanel Element can be published', {
                url: '/element-publishable',
                templateUrl: 'admin-panel/views/elementVariations.html'
            })
            .state('adminPanel ElementVariations publish', {
                url: '/elementVariation-publish',
                templateUrl: 'admin-panel/views/elementVariations.html'
            })
            .state('adminPanel ElementVariations unpublish', {
                url: '/elementVariation-unpublish',
                templateUrl: 'admin-panel/views/elementVariations.html'
            })
            .state('adminPanel Element name', {
                url: '/element-name',
                templateUrl: 'admin-panel/views/elementVariations.html'
            })
            .state('adminPanel Element movable', {
                url: '/element-movable',
                templateUrl: 'admin-panel/views/elements.html'
            })
            .state('adminPanel Element immovable', {
                url: '/element-immovable',
                templateUrl: 'admin-panel/views/elements.html'
            })
            .state('adminPanel Element variation edit', {
                url: '/categories/:category_id/:product_id/:element_id/:variation/edit',
                templateUrl: 'admin-panel/views/editElementVariation.html',
                ncyBreadcrumb: {
                    label: 'Variation',
                    parent: 'adminPanel ElementVariations'
                }
            })
            .state('adminPanel allProducts', {
                url: '/products',
                templateUrl: 'admin-panel/views/allProducts.html'
            })
            .state('adminPanel image upload example page', {
                url: '/image-upload-example',
                templateUrl: 'admin-panel/views/imageUploadExample.html'
            });

        $urlRouterProvider.when('/categories/:category_id/:product_id/:element_id/edit'
            , function($state){
                $state.go('adminPanel Element edit');
            }).rule(function($injector, $location) {
                var path = $location.path();
                var hasTrailingSlash = path[path.length-1] === '/';

                if(hasTrailingSlash) {

                    //if last charcter is a slash, return the same url without the slash
                    var newPath = path.substr(0, path.length - 1);
                    return newPath;
                }

            });
    }
]);
